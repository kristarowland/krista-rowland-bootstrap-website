$(document).ready(function()
{ 
  $("#goto-top").click(function() {
    document.body.scrollTop = 0; 
    document.documentElement.scrollTop = 0; 
  });

  $("[data-fancybox]").fancybox({
      buttons : [
        'fullScreen',
        'close'
      ]});
});