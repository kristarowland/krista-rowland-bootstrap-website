$(document).ready(function()
{
  $(document).mousemove(function(e)
  {
    let mouseX = e.pageX;
    let mouseY = e.pageY;

    let ww = $(window).width();
    let wh = $(window).height();

    let traX = 100 * (mouseX / ww);
    let traY = 100 - (100 * (mouseY / wh));

    traX = 25 + traX / 2;
    traY = 25 + traY / 2;

    $(".title-main").css({"background-position": traX + "%" + traY + "%"});
  });
});

